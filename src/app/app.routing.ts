import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/invoices',
        pathMatch: 'full'
      },
      {
        path: 'invoices',
        loadChildren: () => import('./globaltek/globaltek.module').then(m => m.GlobaltekModule)
      }
    ]
  }
];
