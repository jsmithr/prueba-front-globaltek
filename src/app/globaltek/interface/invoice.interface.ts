import { IInvoiceDetail } from "./invoiceDetail.interface";

export class IInvoice {
    NumeroFactura: number = 0;
    Fecha: string = '';
    TipodePago: string = '';
    Detalle: IInvoiceDetail[] = [];
    DocumentoCliente: string = '';
    NombreCliente: string = '';
    Descuento: number = 0;
    IVA: number = 19;
    id: string = '';
    Total: number = 0;
}