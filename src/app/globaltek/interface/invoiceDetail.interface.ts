export class IInvoiceDetail {
    Item: number = 0;
    Producto: string = '';
    Cantidad: number = 0;
    PrecioUnitario: number = 0;
}