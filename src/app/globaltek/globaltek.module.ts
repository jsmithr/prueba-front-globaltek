import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicesComponent } from './views/invoices/invoices.component';
import { RouterModule } from '@angular/router';
import { GlobaltekRoutes } from './globaltek.routing';
import { MenuItems } from '../shared/menu-items/menu-items';
import { DemoMaterialModule } from '../demo-material-module';
import { SharedModule } from '../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartistModule } from 'ng-chartist';
import { HttpClientModule } from '@angular/common/http';
import { DialogInvoiceComponent } from './views/invoices/dialog-invoices/dialog-invoice.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GlobaltekRoutes),
    DemoMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [  ],
  declarations: [
    InvoicesComponent,
    DialogInvoiceComponent,
  ],
})
export class GlobaltekModule { }
