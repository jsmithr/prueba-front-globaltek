import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { IInvoice } from '../../interface/invoice.interface';
import { IInvoiceDetail } from '../../interface/invoiceDetail.interface';
import { InvoiceService } from '../../service/invoice.service';
import { DialogInvoiceComponent } from './dialog-invoices/dialog-invoice.component';

@Component({
    selector: 'app-invoices',
    templateUrl: './invoices.component.html',
    styleUrls: ['./invoices.component.scss'],
})
export class InvoicesComponent implements AfterViewInit {
    displayedColumns: string[] = ['numberInvoice', 'documentCustomer', 'customer', 'date', 'noIva', 'iva', 'detail'];
    loading = false;
    sequence: number = 0;
    dataSource: MatTableDataSource<IInvoice> = new MatTableDataSource;
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort!: MatSort;

    constructor(public dialog: MatDialog,
        private InvoiceService: InvoiceService,
    ) {
        this.get();
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    openDialog(invoice: IInvoice) {
        const dialogRef = this.dialog.open(DialogInvoiceComponent, { data: { invoice: JSON.parse(JSON.stringify(invoice)), sequence: this.sequence }, disableClose: true, width: '600px', });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                if (result.id == 0)
                    this.dataSource.data.push(result);
                else
                    invoice = result;
            }
        });
    }

    get() {
        this.loading = true;
        this.InvoiceService.get().subscribe((response: any) => {
            let data = response.filter((r: IInvoice) => r.TipodePago == 'contado' || r.TipodePago == 'credito');
            data.map((r: IInvoice) => {
                if (r.NumeroFactura > this.sequence)
                    this.sequence = r.NumeroFactura;

                this.getTotalInvoice(r)
            });

            this.dataSource = new MatTableDataSource(data);
        },
            () => { },
            () => this.loading = false);
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    getTotalInvoice(invoice: IInvoice) {
        let total = 0
        invoice.Detalle.map(id => total += id.PrecioUnitario * id.Cantidad);
        invoice.Total = total
    }

    convertNumber = (n: number) => Number(n).toLocaleString('es-CO');

}
