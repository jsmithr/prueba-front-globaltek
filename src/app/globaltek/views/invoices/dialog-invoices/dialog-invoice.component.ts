import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { IInvoice } from 'src/app/globaltek/interface/invoice.interface';
import { IInvoiceDetail } from 'src/app/globaltek/interface/invoiceDetail.interface';

@Component({
    selector: 'app-dialog-invoice',
    templateUrl: './dialog-invoice.component.html',
    styleUrls: ['./dialog-invoice.component.scss']
})
export class DialogInvoiceComponent implements OnInit {
    date = new FormControl(new Date());
    invoice: IInvoice = new IInvoice();
    displayedColumns: string[] = ['item', 'producto', 'cantidad', 'precioUnitario', 'total', 'action'];
    invoiceDetailNew: IInvoiceDetail = new IInvoiceDetail();
    isEdit: boolean = false;
    sequence: number = 0;
    dataSource: MatTableDataSource<IInvoiceDetail> = new MatTableDataSource;
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort!: MatSort;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private _snackBar: MatSnackBar) {
        if (data.invoice)
            this.invoice = data.invoice;

        if (this.invoice.id == '')
            this.invoice.NumeroFactura = data.sequence + 1;

        if (this.invoice.Fecha != '')
            this.date = new FormControl(new Date(this.invoice.Fecha));

        this.reloadDatasource();
    }

    ngOnInit(): void {

    }

    edit(invoiceDetail: IInvoiceDetail) {
        this.isEdit = true;
        this.invoiceDetailNew = invoiceDetail;
    }

    reloadDatasource() {
        this.dataSource = new MatTableDataSource(this.invoice.Detalle);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }
    getTotalInvoice() {
        let total = 0
        this.invoice.Detalle.map(id => total += id.PrecioUnitario * id.Cantidad);
        this.invoice.Total = total
    }

    addProduct() {
        if (!this.isEdit)
            this.invoice.Detalle.push(this.invoiceDetailNew);

        this.reloadDatasource();
        this.getTotalInvoice();
        this.invoiceDetailNew = new IInvoiceDetail();
    }

    openSnackBar(message: string, styleClass: string = 'bg-success') {
        this._snackBar.open(message, 'Cerrar', {
            duration: 4000,
            panelClass: [styleClass]
        });
    }

    delete(i: number) {
        this.invoice.Detalle.splice(i, 1);
        this.reloadDatasource();
        this.getTotalInvoice();
    }

    convertNumber = (n: number) => Number(n).toLocaleString('es-CO');

}
