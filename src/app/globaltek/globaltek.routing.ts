import { Routes } from '@angular/router';

import { InvoicesComponent } from './views/invoices/invoices.component';

export const GlobaltekRoutes: Routes = [
  {
    path: '',
    component: InvoicesComponent,
    children:[{
      path: 'create',
      component: InvoicesComponent
    }]
  }
];
