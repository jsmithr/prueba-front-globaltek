
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError, timeout, timeoutWith } from 'rxjs/operators';
import { environment as env, environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class Service {
	private id_sesion = 0;

	constructor(private http: HttpClient) {

	}

	protected postApi = (url: {}, params: any = {}, callback = null) => {
		return this.http.post(env.baseUrl + url, params).pipe(catchError(this.handleError));
	};

	protected getApi = (url: {}, parameters = {}) => {
		let newParams = { ...parameters };
		return this.http.get(env.baseUrl + url).pipe(catchError(this.handleError));
	};

	protected handleError(error: HttpErrorResponse) {
		// return an observable with a user friendly message
		return throwError('Error! Algo salió mal.');
	}

	private response = (d: any) => {
		if (!d)
			return;
		return !d.res ? d : JSON.parse(d.res);
	}
}
