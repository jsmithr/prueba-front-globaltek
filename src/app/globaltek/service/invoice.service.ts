import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Service } from './service';

@Injectable({
	providedIn: 'root'
})
export class InvoiceService extends Service {

	constructor(http: HttpClient) {
		super(http);
	}

	get() {
		return this.getApi('/Facturas');
	}
}
